using System;
using System.Linq;
using Xunit;

namespace XUnitTestProject1
{
    public class GreedTest
    {
        public class Greed
        {
            public static int Score(int [] dieValues)
            {
                var dies1 = dieValues.Where(die => die == 1).ToArray();
                var dies2 = dieValues.Where(die => die == 2).ToArray();
                var dies5 = dieValues.Where(die => die == 5).ToArray();

                if (dies1.Length == 3)
                    return 1000;
                if (dies2.Length ==3)
                    return 200;
                if (dies1.Length > 0)
                    return 100;
                if(dies5.Length > 0)
                    return 50;
                return 0;
            }
        }

        [Fact]
        public void TestScroreSingle()
        {
            Assert.Equal(100,Greed.Score(new int[] { 1 }));
            Assert.Equal(100, Greed.Score(new int[] { 1, 2, 3 }));
            Assert.Equal(100, Greed.Score(new int[] { 2, 1, 3 }));
            Assert.Equal(0, Greed.Score(new int[] { 4 }));
            Assert.Equal(50, Greed.Score(new int[] {5}));
        }

        [Fact]
        public void TestScroreTriple()
        {
            Assert.Equal(1000, Greed.Score(new int[] { 1, 1,1 }));
            Assert.Equal(200, Greed.Score(new int[] { 2, 2, 2 }));
        }
    }
}
