using System;
using TemplateLibrary;
using Xunit;

namespace TemplateLibraryTest
{
    public class TemplateTest
    {
        [Fact]
        public void Test_Assert_Value_True()
        {
            TemplateClass template = new TemplateClass();
            Assert.True(template.value);
        }

        [Fact]
        public void Test_Assert_Value_False()
        {
            TemplateClass template = new TemplateClass();
            Assert.False(template.value);
        }
    }
}
